# Advent of Code 2022

This is my participation to [Advent of Code 2022](https://adventofcode.com/2022), written in Scala.

My main objective was to learn Scala and see if I like it (see my thoughts below). It's likely I haven't always used the best or most idiomatic ways of doing things, and I only had time to do the first 12 days.

To run:

```
$ sbt run
```

Or, if you only want the answers for some days, say the 9th and 12th:

```
$ sbt "run 9 18"
```

You can also run `sbt test` to check the solutions against the examples given by Advent of Code.

My input files are in the `inputs` folder. The code assumes all inputs are valid (i.e. doesn't check is they are well-formed).

## My thoughts on Scala after Advent of Code

My experience with Scala was quite good! My main complaint is that there are too many ways of doing things, and there don't seem to be a unique, idiomatic way.

### What I liked

- The functional features like iterators or `Option`s.
- Some parts of the syntax, especially the square brackets for generics, which I find really better than angle brackets.

### What I disliked

Things I noticed or banged my head on, in no particular order:

- There seems to be too many ways of doing things:
	- Classes vs. case classes.
	- Class inheritance vs. trait vs type class pattern vs. extension methods vs. companion object vs. …
	- `ArraySeq` vs `Array` vs `Vector` vs. … For example, for day 5 and the stack of crates, I used a `Stack`, but I could as well have used a `List`.
- Everything is public by default. (And it's not even consistent, since in the case of `class C(attr: Int)`, `attr` is private by default.
- Some inconsistencies (in my opinion) in syntax:
	- `=` when defining methods, but `:` when defining classes, traits and all the rest. It's understandable since for methods, a colon is used to indicate the return type, but still a bit confusing. I guess this stems from Scala 2 using braces, and the indentation-based syntax has been added in Scala 3?
	- To make an attribute public, we have to write `class C(val attr: Int)` and not `class C(public attr: Int)` like for the things declared in the class' body.
- No control statements/expressions (`break`, `continue`…). Instead we have to use things in `scala.util.control`. In particular, we cannot return in a `for do` loop because its content is considered to be the closure of a `.foreach` even if the syntax suggests otherwise.
- No pattern matching on ranges.
- No pipe operator.

### In conclusion

I don't really know. On one hand, I find it better than, say, Python. On the other hand, I find it a bit too complicated and fiddly for quick projects. All in all, I did find it an interesting language.
