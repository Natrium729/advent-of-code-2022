package xyz.natrium729.adventofcode2022

import scala.collection.mutable as mut

private class HeightMap(
  contents: Array[Int],
  height: Int,
  width: Int,
  start: (Int, Int),
  goal: (Int, Int)
):
  private def coordsToIndex(line: Int, col: Int): Int =
    return line * width + col

  private def indexToCoords(i: Int): (Int, Int) =
    (i / width, i % width)

  private def heightAt(line: Int, col: Int): Int =
    contents(coordsToIndex(line, col))

  private def reachableNeighbours(line: Int, col: Int, part2: Boolean = false): Iterator[(Int, Int)] =
    val left = if col - 1 >= 0 then Some(line, col - 1) else None
    val right = if col + 1 < width then Some(line, col + 1) else None
    val up = if line - 1 >= 0 then Some(line - 1, col) else None
    val down = if line + 1 < height then Some(line + 1, col) else None
    val currentHeight = heightAt(line, col)
    left.iterator.concat(right).concat(up).concat(down)
      .filter((otherLine, otherCol) =>
        if part2 then // We're going in reverse, from goal to start.
          currentHeight <= heightAt(otherLine, otherCol) + 1
        else
          heightAt(otherLine, otherCol) <= currentHeight + 1
      )

  // Dijkstra's algorithm.
  // There're a lot a wasteful conversions between indices and coordinates, but it'll do.
  def stepsToGoal(part2: Boolean = false): Int =
    val distances = Array.ofDim[Int](contents.length)
    val queue = mut.HashSet[Int]()
    for i <- 0 until contents.length do
      distances(i) = Int.MaxValue
      queue.add(i)
    if part2 then // Start from the end.
      distances(coordsToIndex(goal(0), goal(1))) = 0
    else
      distances(coordsToIndex(start(0), start(1))) = 0

    import scala.util.control.NonLocalReturns.*
    returning {
      // Should always return since we'll stop when reaching the goal.
      while true do
        val nextI = queue.minBy(i => distances(i))
        val nextDist = distances(nextI)
        queue.remove(nextI)

        val (line, col) = indexToCoords(nextI)
        for neighbour <- reachableNeighbours(line, col, part2)
          .map((otherLine, otherCol) => coordsToIndex(otherLine, otherCol))
          .filter(i => queue.contains(i))
        do
          distances(neighbour) = distances(nextI) + 1
          if
            (part2 && contents(neighbour) == 0) ||
            (!part2 && neighbour == coordsToIndex(goal(0), goal(1)))
          then
            throwReturn(distances(neighbour))

      throw Exception("unreachable in stepsToGoal")
    }


final case class Day12(input: Option[String] = None) extends Day(input):
  override val number = 12

  override def answers: (Int, Int) =
    val input = getInput()
    val height = input.linesIterator.size
    val width = input.linesIterator.next().length()
    var start = (0, 0)
    var goal = (0, 0)
    val contents = input.linesIterator
      .zipWithIndex
      .flatMap((line, y) =>
        line.view.zipWithIndex.map((ch, x) => ch match
          case 'S' =>
            start = (y, x)
            0
          case 'E' =>
            goal = (y, x)
            25
          case ch if ('a' to 'z').contains(ch) => ch - 'a'
          case _ => throw Exception("invalid height")
        )
      ).toArray
    val map = HeightMap(contents, height, width, start, goal)

    // Part 1

    val a1 = map.stepsToGoal()

    // Part 2

    val a2 = map.stepsToGoal(true)

    (a1, a2)
