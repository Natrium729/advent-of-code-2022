package xyz.natrium729.adventofcode2022

private enum Instruction:
  case Addx(value: Int)
  case Noop

  def cycles: Int = this match
    case Instruction.Addx(_) => 2
    case Instruction.Noop => 1

private def isCheckedCycle(cycle: Int): Boolean =
  cycle match
    case 20 | 60 | 100 | 140 | 180 | 220 => true
    case _ => false

private object Instruction:
  def fromString(str: String): Instruction =
    val words = str.strip().split("\\s")
    words(0) match
      case "noop" => Instruction.Noop
      case "addx" => Instruction.Addx(words(1).toInt)
      case _ => throw new Exception("invalid instruction")

private case class Cpu (
  x: Int = 1,
  cycle: Int = 0,
  strengthSum: Int = 0,
  crt: String = ""
):

  def nextCycle: Cpu =
    var next = cycle + 1

    var newSum = strengthSum
    if isCheckedCycle(next) then
      newSum += next * x

    var newCrt = crt
    // Below, 41 and not 40 because we have to account
    // for the '\n' at the start of each line.
    if crt.length % 41 == 0 then newCrt += "\n"
    val drawnPos = (next - 1) % 40
    if x - 1 <= drawnPos && drawnPos <= x + 1 then
      newCrt += "#"
    else
      newCrt += "."

    this.copy(cycle = next, strengthSum = newSum, crt = newCrt)

  def follow(instruction: Instruction): Cpu =
    instruction match
      case Instruction.Addx(value) =>
        val updated = this.nextCycle.nextCycle
        updated.copy(x = x + value)
      case Instruction.Noop =>
        this.nextCycle

final case class Day10(input: Option[String] = None) extends Day(input):
  override val number = 10

  override def answers: (Int, String) =
    val cpu = getInputLines().map(Instruction.fromString)
      .foldLeft(Cpu())((cpu, instruction) => cpu.follow(instruction))

    // Part 1

    val a1 = cpu.strengthSum

    // Part 2

    val a2 = cpu.crt

    (a1, a2)
