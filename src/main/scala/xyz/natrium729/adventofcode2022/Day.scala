package xyz.natrium729.adventofcode2022

/**
  * Defines the common behaviour for each days.
  *
  * @param input If not `None`, it will be used as the input. Otherwise, the input will be retrieved from the file for this day. Useful when testing some specific inputs.
  */
trait Day(input: Option[String]):

  /** This day's number. Used to open the right input file. */
  val number = 0

  /* The path of the input file. */
  def inputPath = s"./inputs/day$number.txt"

  final def source: io.Source = input match
    case None => io.Source.fromFile(inputPath, "utf8")
    case Some(contents) => io.Source.fromString(contents)

  /** Gets the whole input as a String. */
  def getInput(): String =
    source.mkString

  /** Returns an iterator running over each line of the input. */
  def getInputLines(): Iterator[String] =
    source.getLines()

  def answers: (Any, Any) = ("", "")

  def showAnswers(): Unit =
    val (a1, a2) = answers
    println(s"Day $number.1: $a1\nDay $number.2: $a2")
