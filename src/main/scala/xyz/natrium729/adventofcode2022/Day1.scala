package xyz.natrium729.adventofcode2022

import scala.io.Source

final case class Day1(input: Option[String] = None) extends Day(input):
  override val number = 1

  override def answers: (Int, Int) =
    val calories = getInput().strip().split("\r?\n\r?\n")
      .map(par => par.linesIterator.map(_.toInt).sum)
      .sortInPlaceWith(_ > _)
    val a1 = calories.head
    val a2 = calories.view.take(3).sum

    (a1, a2)
