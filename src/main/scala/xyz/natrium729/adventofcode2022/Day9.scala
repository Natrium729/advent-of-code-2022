package xyz.natrium729.adventofcode2022

import scala.collection.mutable as mut

private enum Direction:
  case Up(distance: Int)
  case Right(distance: Int)
  case Down(distance: Int)
  case Left(distance: Int)

private object Direction:
  def fromTuple(dir: Char, dist: Int): Direction = dir match
    case 'U' => Up(dist)
    case 'R' => Right(dist)
    case 'D' => Down(dist)
    case 'L' => Left(dist)

private case class Coordinate(x: Int, y: Int):
  def up = Coordinate(x, y + 1)
  def down = Coordinate(x, y - 1)
  def right = Coordinate(x + 1, y)
  def left = Coordinate(x - 1, y)

private class RopeSimulation(length: Int):
  val rope = Array.fill(length) { Coordinate(0, 0) }
  // The starting position is visited.
  val visited = mut.Set(Coordinate(0, 0))

  def updateTail(): Unit =
    for IndexedSeq(i, j) <- (0 until length).sliding(2) do
      (rope(i).x - rope(j).x, rope(i).y - rope(j).y) match
        case (2, 0) => rope(j) = rope(j).right
        case (-2, 0) => rope(j) = rope(j).left
        case (0, 2) => rope(j) = rope(j).up
        case (0, -2) => rope(j) = rope(j).down
        case (1, 2) | (2, 1) => rope(j) = rope(j).right.up
        case (1, -2) | (2, -1) => rope(j) = rope(j).right.down
        case (-1, -2) | (-2, -1) => rope(j) = rope(j).left.down
        case (-1, 2) | (-2, 1) => rope(j) = rope(j).left.up
        case (2, 2) => rope(j) = rope(j).right.up
        case (2, -2) => rope(j) = rope(j).right.down
        case (-2, -2) => rope(j) = rope(j).left.down
        case (-2, 2) => rope(j) = rope(j).left.up
        case _ => // Do nothing.

    visited += rope.last


  def move(dir: Direction): Unit = dir match
    case Direction.Up(distance) =>
      for _ <- 0 until distance do
        rope(0) = rope(0).up
        updateTail()
    case Direction.Right(distance) =>
      for _ <- 0 until distance do
        rope(0) = rope(0).right
        updateTail()
    case Direction.Down(distance) =>
      for _ <- 0 until distance do
        rope(0) = rope(0).down
        updateTail()
    case Direction.Left(distance) =>
      for _ <- 0 until distance do
        rope(0) = rope(0).left
        updateTail()

final case class Day9(input: Option[String] = None) extends Day(input):
  override val number = 9

  override def answers: (Int, Int) =
    def directions: Iterator[Direction] =
      getInputLines().map(line =>
        val dir = line.split("\\s")
        Direction.fromTuple(dir(0).charAt(0), dir(1).toInt)
      )

    // Part 1

    val simulation = RopeSimulation(2)
    directions.foreach(dir => simulation.move(dir))

    val a1 = simulation.visited.size

    // Part 2

    val simulation2 = RopeSimulation(10)
    directions.foreach(dir => simulation2.move(dir))

    val a2 = simulation2.visited.size

    (a1, a2)
