package xyz.natrium729.adventofcode2022

final case class Day4(input: Option[String] = None) extends Day(input):
  override val number = 4

  override def answers: (Int, Int) =
    def rangeToTuple(r: String): (Int, Int) =
      val Array(r1, r2) = r.split("-").map(_.toInt)
      (r1, r2)

    val pairs = getInputLines()
      .map(line =>
        val Array(r1, r2) = line.split(",").map(rangeToTuple)
        (r1, r2)
      ).toList

    // Part 1

    def oneFullyContainsOther(r1: (Int, Int), r2: (Int, Int)): Boolean =
      (r1(0) >= r2(0) && r1(1) <= r2(1)) || (r1(0) <= r2(0) && r1(1) >= r2(1))

    val a1 = pairs.count(ranges => oneFullyContainsOther(ranges(0), ranges(1)))

    // Part 2

    def oneOverlapsOther(r1: (Int, Int), r2: (Int, Int)): Boolean =
      (r1(0) >= r2(0) && r1(0) <= r2(1))
      || (r1(1) >= r2(0) && r1(1) <= r2(1))
      || (r2(0) >= r1(0) && r2(0) <= r1(1))
      || (r2(1) >= r1(0) && r2(1) <= r1(1))

    // The line number should always be a multiple of 3.
    val a2 = pairs.count(ranges => oneOverlapsOther(ranges(0), ranges(1)))

    (a1, a2)
