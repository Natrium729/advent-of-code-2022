package xyz.natrium729.adventofcode2022

import scala.collection.mutable.Stack

private class CrateStacks private ():
  private var stacks = Array(Stack[Char]())

  def this(size: Int) =
    this()
    stacks = Array.fill(size) { Stack() }

  def pushCrate(i: Int, crate: Char): Unit = stacks(i).push(crate)

  def popCrate(i: Int): Char = stacks(i).pop()

  def follow(step: Step): Unit =
    for _ <- 0 until step.quantity do
      val crate = popCrate(step.from)
      pushCrate(step.to, crate)

  // For part 2 of the day.
  def follow2(step: Step): Unit =
    // Push crates as one.
    stacks(step.from).view.take(step.quantity)
      .reverse
      .foreach(crate => pushCrate(step.to, crate))
    // Pop crates from the starting stack.
    for _ <- 0 until step.quantity do popCrate(step.from)

  def topCrates: String =
    // Turns out the top of the stack is at index 0.
    stacks.flatMap(_.headOption).mkString

private def stacksFromDrawing(drawing: String): CrateStacks =
  val lines = drawing.split("\r?\n")

  val lastIndex = lines.last.strip().split(raw"\s+").last.toInt
  val stacks = CrateStacks(lastIndex)

  val x = lines
    .reverseIterator // To start from the bottom crates.
    .drop(1) // Drop the line with the indices of the stack.
    .foreach(line =>
      line.view
        .grouped(4) // Each crates appear in a 4-character group.
        .map(x=> x.drop(1).head) // The crate's letter is at index 1 of each group.
        .zipWithIndex
        .filter(_(0) != ' ') // Only if a crate is present.
        .foreach(tuple =>
          stacks.pushCrate(tuple(1), tuple(0))
        )
    )

  stacks

private final case class Step(quantity: Int, from: Int, to: Int)

final case class Day5(input: Option[String] = None) extends Day(input):
  override val number = 5

  override def answers: (String, String) =
    val Array(crates, procedure) = getInput().split("\r?\n\r?\n")
    val stacks = stacksFromDrawing(crates)

    val instructions = procedure.linesIterator
      .map(line =>
        val ints = line.strip().split(" ").flatMap(_.toIntOption)
        Step(ints(0), ints(1) - 1, ints(2) - 1)
      ).toList

    instructions.foreach(stacks.follow)
    val a1 = stacks.topCrates

    // A new set of stacks in their original position.
    // It's a bit wasteful to reparse the input, but it'll do.
    val stacks2 = stacksFromDrawing(crates)
    instructions.foreach(stacks2.follow2)
    val a2 = stacks2.topCrates

    (a1, a2)
