package xyz.natrium729.adventofcode2022

import scala.collection.mutable as mut

private enum Operand:
  case Old
  case Constant(n: Long)

  def fromOld(old: Long): Long = this match
    case Operand.Old => old
    case Operand.Constant(n) => n

private object Operand:
  def fromString(s: String): Operand = s match
    case "old" => Operand.Old
    case other => Operand.Constant(other.toInt)

private enum Operation:
  case Plus(o1: Operand, o2: Operand)
  case Times(o1: Operand, o2: Operand)

  def execute(worry: Long): Long = this match
    case Operation.Plus(o1, o2) => o1.fromOld(worry) + o2.fromOld(worry)
    case Operation.Times(o1, o2) => o1.fromOld(worry) * o2.fromOld(worry)


private class Monkey(
  id: Int,
  val items: mut.ArrayDeque[Long],
  val operation: Operation,
  val divisor: Int,
  val ifTrue: Int,
  val ifFalse: Int,
):
  var totalInspected: Long = 0

private object Monkey:
  def fromInput(input: String): Monkey =
    val lines = input.linesIterator.map(_.strip())

    val id = lines.next().stripPrefix("Monkey ").stripSuffix(":").toInt

    val items = lines.next().stripPrefix("Starting items: ")
      .split(", ")
      .view
      .map(_.toLong)
      .to(mut.ArrayDeque)

    val operationTokens = lines.next().split("\\s+")
    val o1 = Operand.fromString(operationTokens(3))
    val o2 = Operand.fromString(operationTokens(5))
    val operation = operationTokens(4) match
      case "+" => Operation.Plus(o1, o2)
      case "*" => Operation.Times(o1, o2)

    val divisor = lines.next().split("\\s+").last.toInt

    // We assume the "if true" case always comes before the "if false" case.
    val ifTrue = lines.next().split("\\s+").last.toInt
    val ifFalse = lines.next().split("\\s+").last.toInt

    Monkey(id, items, operation, divisor, ifTrue, ifFalse)

private def round(monkeys: Array[Monkey], part2: Option[Int] = None) =
  for monkey <- monkeys do
    while !monkey.items.isEmpty do
      val item = monkey.items.removeHead()
      val newItem = if part2.isDefined then
        // Modulo to keep the worry in check.
        monkey.operation.execute(item) % part2.get
      else
        monkey.operation.execute(item) / 3
      monkey.totalInspected += 1
      if newItem % monkey.divisor == 0 then
        monkeys(monkey.ifTrue).items.append(newItem)
      else
        monkeys(monkey.ifFalse).items.append(newItem)

final case class Day11(input: Option[String] = None) extends Day(input):
  override val number = 11

  override def answers: (Long, Long) =
    // Part 1
    val monkeys = getInput().split("\r?\n\r?\n")
      .map(Monkey.fromInput) // We assume the monkeys are in order in the input.

    for _ <- 0 until 20 do
      round(monkeys)

    val a1 = monkeys.view
      .map(_.totalInspected)
      .sorted
      .reverse
      .take(2)
      .product

    // Part 2

    // Although a bit wasteful, we parse again to have fresh monkeys.
    val monkeys2 = getInput().split("\r?\n\r?\n")
      .map(Monkey.fromInput)

    val divisorProduct = monkeys2.view.map(_.divisor).product
    for _ <- 0 until 10_000 do
      round(monkeys2, Some(divisorProduct))

    val a2 = monkeys2.view
      .map(_.totalInspected)
      .sorted
      .reverse
      .take(2)
      .product

    (a1, a2)
