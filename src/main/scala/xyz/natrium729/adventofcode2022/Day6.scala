package xyz.natrium729.adventofcode2022

final case class Day6(input: Option[String] = None) extends Day(input):
  override val number = 6

  override def answers: (Int, Int) =
    // Part 1

    val data = getInput().strip()

    // Using sliding windows might not be the most efficient,
    // because in some case we know we can skip multiple indices.
    // For example, with "abxx", we can skip 2 indices
    // because "xx" wil be in the next 2 windows.
    val (_, i1) = data.view.sliding(4)
      .zipWithIndex
      // Converting to a map is not the most efficient, I guess, but it'll do.
      .find((window, i) => window.toSet.size == 4)
      .get // We're sure there'll be an answer.

    val a1 = i1 + 4 // Plus 4 to get the index at the end of the window.

    // Part 2

    // Copy-paste of the above, but with windows of size 14.
    // (We could've have extracted the logic in a method, I guess. Anyway.)
    // The performance issue mentioned in part 1 should be rendered worse,
    // But it remains near instant on my machine.
    val (_, i2) = data.view.sliding(14)
      .zipWithIndex
      .find((window, i) => window.toSet.size == 14)
      .get

    val a2 = i2 + 14 // Plus 14 to get the index at the end of the window.

    (a1, a2)
