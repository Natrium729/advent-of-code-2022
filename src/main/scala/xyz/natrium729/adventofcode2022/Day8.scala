package xyz.natrium729.adventofcode2022

private case class Tree(line: Int, column: Int, height: Int)

private case class Forest private (map: Array[Tree], height: Int, width: Int):
  def lines: Iterator[Array[Tree]] =
    map.grouped(width)

  def columns: Iterator[Array[Tree]] =
    val cols = for col <- 0 until width yield
      map.grouped(width)
        .map(_(col))
        .toArray
    cols.iterator

  /** Returns the tree at the given coordinates. */
  def get(line: Int, col: Int): Tree =
    map(line * width + col)

  /** Returns all the trees east from the one at the given coordinates */
  def eastFrom(line: Int, col: Int): IndexedSeq[Tree] =
    (col + 1 until width).map(visitedCol => get(line, visitedCol))

  /** Returns all the trees west from the one at the given coordinates */
  def westFrom(line: Int, col: Int): IndexedSeq[Tree] =
    (col - 1 to 0 by -1).map(visitedCol => get(line, visitedCol))

  /** Returns all the trees south from the one at the given coordinates */
  def southFrom(line: Int, col: Int): IndexedSeq[Tree] =
    (line + 1 until height).map(visitedLine => get(visitedLine, col))

  /** Returns all the trees north from the one at the given coordinates */
  def northFrom(line: Int, col: Int): IndexedSeq[Tree] =
    (line - 1 to 0 by -1).map(visitedLine => get(visitedLine, col))

  def scenicScore(line: Int, col: Int): Int =
    // I tried to do it in a functional way with iterators,
    // but we'd need some kind of `takeUntil`,
    // Or we should use `takeWhile(_.height < tree.height)`,
    // Then make a check on the next one to see if its height is equal.
    // It's just cleaner with a imperative way, in my opinion.

    import scala.util.control.Breaks.{break, breakable}

    val tree = get(line, col)
    var east = 0
    breakable {
      for visited <- eastFrom(line, col) do
        east += 1
        if visited.height >= tree.height then break()
    }
    var west = 0
    breakable {
      for visited <- westFrom(line, col) do
        west += 1
        if visited.height >= tree.height then break()
    }
    var south = 0
    breakable {
      for visited <- southFrom(line, col) do
        south += 1
        if visited.height >= tree.height then break()
    }
    var north = 0
    breakable {
      for visited <- northFrom(line, col) do
        north += 1
        if visited.height >= tree.height then break()
    }

    // Since it's a product, we could return early as soon as we find a zero,
    // Instead of checking the rest of the directions.
    // But I haven't done that.

    east * west * south * north

private object Forest:
  def fromString(str: String): Forest =
    val stripped = str.strip()
    val width = stripped.linesIterator.next().length()
    val map = stripped.linesIterator
      .zipWithIndex
      .flatMap((line, y) =>
        line.zipWithIndex.map((ch, x) => Tree(y, x, ch.asDigit))
      ).toArray
    val height = map.length / width
    Forest(map, height, width)


final case class Day8(input: Option[String] = None) extends Day(input):
  override val number = 8

  override def answers: (Int, Int) =
    val forest = Forest.fromString(getInput())

    // Part 1

    def visibleTrees(trees: Iterator[Tree]): Iterator[Tree] =
      var highest = -1
      trees.filter(tree =>
        if tree.height > highest then
          highest = tree.height
          true
        else
          false
      )

    val linesFromWest = forest.lines
      .flatMap(line => visibleTrees(line.iterator))
    val linesFromEast = forest.lines
      .flatMap(line => visibleTrees(line.reverseIterator))
    val columnsFromNorth = forest.columns
      .flatMap(col => visibleTrees(col.iterator))
    val columnsFromSouth = forest.columns
      .flatMap(col => visibleTrees(col.reverseIterator))
    // Edges are always visible.
    val firstLine = forest.lines.next().iterator
    val lastLine = forest.lines.toArray.last.iterator
    val firstCol = forest.columns.next().iterator
    val lastCol = forest.columns.toArray.last.iterator

    val a1 = linesFromWest
      .concat(linesFromEast)
      .concat(columnsFromNorth)
      .concat(columnsFromSouth)
      .concat(firstLine)
      .concat(lastLine)
      .concat(firstCol)
      .concat(lastCol)
      .toSet // To remove duplicates.
      .size

    // Part 2

    val scores = for
      line <- 0 until forest.width
      col <- 0 until forest.height
    yield
      forest.scenicScore(line, col)

    val a2 = scores.max

    (a1, a2)
