package xyz.natrium729.adventofcode2022

import scala.collection.mutable as mut

// As the adage says, "everything is a file", even folders.
private class File(
  val name: String,
  var weight: Int = 0,
  children: mut.Map[String, File] = mut.Map(),
  // Since the only file without parent is the root,
  // it will be easier to set it to null instead of wrangling with Options.
  val parent: File,
):
  // A folder has children or a weight of zero.
  // (And we hope there are no empty files with a weight of zero.)
  def isFolder = !children.isEmpty || weight == 0

  def getChild(name: String): File =
    children(name)

  def addChild(name: String, weight: Int = 0): Unit =
    val newChild = File(name, weight = weight, parent = this)
    children(newChild.name) = newChild
    // Avoid to recursively update the weight if there's no need.
    if newChild.weight != 0 then
      updateWeight(newChild.weight)

  /** Update the weight of this file and its parent recursively. */
  private def updateWeight(add: Int): Unit =
    weight += add
    if name != "/" then
      parent.updateWeight(add)

  /** An iterator yielding this file and all its descendants. */
  def iter: Iterator[File] =
    Iterable.single(this).concat(children.valuesIterator.flatMap(_.iter)).iterator

final case class Day7(input: Option[String] = None) extends Day(input):
  override val number = 7

  override def answers: (Int, Int) =
    val root = File("/", parent = null)
    var cwd = root

    // First we build the file tree, computing all the weights.
    for l <- getInputLines() do
      val line = l.strip()
      if line.startsWith("$ ") then
        val cmd = l.substring(2)
        if cmd.startsWith("cd ") then
          val destPath = cmd.substring(3)
          cwd = destPath match
            case "/" => root
            case ".." => cwd.parent
            // Will always be a simple name and never a full relative path.
            // Also, we assume this particular child
            // will have already be set by a previous `ls`.
            case name => cwd.getChild(name)
        else if cmd == "ls" then
          // Nothing to do because we simply assume that
          // lines not starting with the prompt is a file we are listing.
          ;
      else // No prompt means it's a file we are listing.
        line.split("\\s+") match
            case Array("dir", name) => cwd.addChild(name);
            case Array(w, name) => cwd.addChild(name, weight = w.toInt)

    // Part 1

    val a1 = root.iter
      .filter(file => file.isFolder && file.weight <= 100_000)
      .map(_.weight)
      .sum

    // Part 2

    val available = 70_000_000 - root.weight
    val toBeRegained = 30_000_000 - available
    val a2 = root.iter
      .filter(file => file.isFolder && file.weight >= toBeRegained)
      .map(_.weight)
      .min

    (a1, a2)
