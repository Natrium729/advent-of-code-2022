package xyz.natrium729.adventofcode2022

private enum Outcome:
  case Lose, Draw, Win

  def toScore: Int = this match
    case Lose => 0
    case Draw => 3
    case Win => 6

private object Outcome:
  def fromLetter(c: Char) = c match
    case 'X' => Lose
    case 'Y' => Draw
    case 'Z' => Win

import Outcome.*

private enum Move:
  case Rock, Paper, Scissors

  def outcomeAgainst(opponent: Move): Outcome =
    // Surely there's a formula we could use to get the outcome of a match.
    // I'm sure it's not complicated, but I'm just going to hardcode everything that isn't a draw.
    if this == opponent then return Draw
    (this, opponent) match
      case (Rock, Scissors) | (Paper, Rock) | (Scissors, Paper) => Win
      case _ => Lose

  /** Returns the move I have to play against this to get the given outcome. */
  def toPlayForOutcome(outcome: Outcome): Move =
    if outcome == Draw then return this
    (this, outcome) match
      case (Rock, Lose) => Scissors
      case (Rock, Win) => Paper
      case (Paper, Lose) => Rock
      case (Paper, Win) => Scissors
      case (Scissors, Lose) => Paper
      case (Scissors, Win) => Rock
      case other => throw MatchError(other)

  def toScore: Int = this match
    case Rock => 1
    case Paper => 2
    case Scissors => 3

private object Move:
  def fromLetter(c: Char): Move = c match
    case 'A' | 'X' => Rock
    case 'B' | 'Y' => Paper
    case 'C' | 'Z' => Scissors

final case class Day2(input: Option[String] = None) extends Day(input):
  override val number = 2

  override def answers: (Int, Int) =

    // Part 1

    val processedInput = getInputLines()
      .map(line => line.split(" ").map(_.charAt(0)))
      .toList

    val a1 = processedInput
      .map(round =>
        val opponent = Move.fromLetter(round(0))
        val me = Move.fromLetter(round(1))
        me.toScore + me.outcomeAgainst(opponent).toScore
      ).sum

    // Part 2

    val a2 = processedInput
      .map(round =>
        val outcome = Outcome.fromLetter(round(1))
        val me = Move.fromLetter(round(0)).toPlayForOutcome(outcome)
        me.toScore + outcome.toScore
      ).sum

    (a1, a2)
