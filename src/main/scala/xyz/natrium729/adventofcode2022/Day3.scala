package xyz.natrium729.adventofcode2022

private def itemPriority(c: Char): Int = c match
  case c if c >= 'a' && c <= 'z' => c - 'a' + 1 // i.e. 'a' => 1, 'b' => 2...
  case c if c >= 'A' && c <= 'Z' => c - 'A' + 27 // i.e. 'A' => 27, 'B' => 28...

final case class Day3(input: Option[String] = None) extends Day(input):
  override val number = 3

  override def answers: (Int, Int) =
    // Part 1

    val a1 = getInputLines()
      .map(line =>
        val (b1, b2) = line.splitAt(line.length() / 2)
        val inBoth = b1.intersect(b2).charAt(0) // There should be only one.
        itemPriority(inBoth)
      ).sum

    // Part 2

    // The line number should always be a multiple of 3.
    val a2 = getInputLines().grouped(3)
      .map(group =>
        val common = group(0).intersect(group(1)).intersect(group(2))
        itemPriority(common.charAt(0))
      ).sum

    (a1, a2)
