import xyz.natrium729.adventofcode2022.*

val days = Array(
  Day1(),
  Day2(),
  Day3(),
  Day4(),
  Day5(),
  Day6(),
  Day7(),
  Day8(),
  Day9(),
  Day10(),
  Day11(),
  Day12(),
)

@main def adventOfCode2022(args: String*): Unit =
  if args.length == 0 then
    days.foreach(_.showAnswers())
  else
    args.view
      .flatMap(_.toIntOption)
      .foreach(n => days(n - 1).showAnswers())
