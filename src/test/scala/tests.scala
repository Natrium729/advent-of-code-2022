import xyz.natrium729.adventofcode2022.*

class DaySuite extends munit.FunSuite:
  test("Day 1") {
    val (a1, a2) = Day1(Some(
      """1000
        |2000
        |3000
        |
        |4000
        |
        |5000
        |6000
        |
        |7000
        |8000
        |9000
        |
        |10000""".stripMargin
    )).answers

    assertEquals(a1, 24_000)
    assertEquals(a2, 45_000)
  }

  test("Day 2") {
    val (a1, a2) = Day2(Some("A Y\nB X\nC Z")).answers

    assertEquals(a1, 15)
    assertEquals(a2, 12)
  }

  test("Day 3") {
    val (a1, a2) = Day3(Some(
      """vJrwpWtwJgWrhcsFMMfFFhFp
        |jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
        |PmmdzqPrVvPwwTWBwg
        |wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
        |ttgJtRGJQctTZtZT
        |CrZsJsPPZsGzwwsLwLmpwMDw""".stripMargin
    )).answers

    assertEquals(a1, 157)
    assertEquals(a2, 70)
  }

  test("Day 4") {
    val (a1, a2) = Day4(Some(
      """2-4,6-8
        |2-3,4-5
        |5-7,7-9
        |2-8,3-7
        |6-6,4-6
        |2-6,4-8""".stripMargin
    )).answers

    assertEquals(a1, 2)
    assertEquals(a2, 4)
  }

  test("Day 5") {
    val (a1, a2) = Day5(Some(
      """    [D]    
        |[N] [C]    
        |[Z] [M] [P]
        | 1   2   3 

        |move 1 from 2 to 1
        |move 3 from 1 to 3
        |move 2 from 2 to 1
        |move 1 from 1 to 2""".stripMargin
    )).answers

    assertEquals(a1, "CMZ")
    assertEquals(a2, "MCD")
  }

  test("Day 6") {
    val (a1, a2) = Day6(Some("mjqjpqmgbljsphdztnvjfqwrcgsmlb")).answers
    assertEquals(a1, 7)
    assertEquals(a2, 19)

    val (b1, b2) = Day6(Some("bvwbjplbgvbhsrlpgdmjqwftvncz")).answers
    assertEquals(b1, 5)
    assertEquals(b2, 23)

    val (c1, c2) = Day6(Some("nppdvjthqldpwncqszvftbrmjlhg")).answers
    assertEquals(c1, 6)
    assertEquals(c2, 23)

    val (d1, d2) = Day6(Some("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg")).answers
    assertEquals(d1, 10)
    assertEquals(d2, 29)

    val (e1, e2) = Day6(Some("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw")).answers
    assertEquals(e1, 11)
    assertEquals(e2, 26)
  }

  test("Day 7") {
    val (a1, a2) = Day7(Some(
      """$ cd /
        |$ ls
        |dir a
        |14848514 b.txt
        |8504156 c.dat
        |dir d
        |$ cd a
        |$ ls
        |dir e
        |29116 f
        |2557 g
        |62596 h.lst
        |$ cd e
        |$ ls
        |584 i
        |$ cd ..
        |$ cd ..
        |$ cd d
        |$ ls
        |4060174 j
        |8033020 d.log
        |5626152 d.ext
        |7214296 k""".stripMargin
    )).answers
    assertEquals(a1, 95_437)
    assertEquals(a2, 24_933_642)
  }

  test("Day 8") {
    val (a1, a2) = Day8(Some("30373\n25512\n65332\n33549\n35390")).answers
    assertEquals(a1, 21)
    assertEquals(a2, 8)
  }

  test("Day 9") {
    val (a1, a2) = Day9(Some(
      """R 4
        |U 4
        |L 3
        |D 1
        |R 4
        |D 1
        |L 5
        |R 2""".stripMargin
    )).answers
    assertEquals(a1, 13)
    assertEquals(a2, 1)

    val (b1, b2) = Day9(Some(
      """R 5
        |U 8
        |L 8
        |D 3
        |R 17
        |D 10
        |L 25
        |U 20""".stripMargin
    )).answers
    assertEquals(b2, 36)
  }

  test("Day 10") {
    val (a1, a2) = Day10(Some(
      """addx 15
        |addx -11
        |addx 6
        |addx -3
        |addx 5
        |addx -1
        |addx -8
        |addx 13
        |addx 4
        |noop
        |addx -1
        |addx 5
        |addx -1
        |addx 5
        |addx -1
        |addx 5
        |addx -1
        |addx 5
        |addx -1
        |addx -35
        |addx 1
        |addx 24
        |addx -19
        |addx 1
        |addx 16
        |addx -11
        |noop
        |noop
        |addx 21
        |addx -15
        |noop
        |noop
        |addx -3
        |addx 9
        |addx 1
        |addx -3
        |addx 8
        |addx 1
        |addx 5
        |noop
        |noop
        |noop
        |noop
        |noop
        |addx -36
        |noop
        |addx 1
        |addx 7
        |noop
        |noop
        |noop
        |addx 2
        |addx 6
        |noop
        |noop
        |noop
        |noop
        |noop
        |addx 1
        |noop
        |noop
        |addx 7
        |addx 1
        |noop
        |addx -13
        |addx 13
        |addx 7
        |noop
        |addx 1
        |addx -33
        |noop
        |noop
        |noop
        |addx 2
        |noop
        |noop
        |noop
        |addx 8
        |noop
        |addx -1
        |addx 2
        |addx 1
        |noop
        |addx 17
        |addx -9
        |addx 1
        |addx 1
        |addx -3
        |addx 11
        |noop
        |noop
        |addx 1
        |noop
        |addx 1
        |noop
        |noop
        |addx -13
        |addx -19
        |addx 1
        |addx 3
        |addx 26
        |addx -30
        |addx 12
        |addx -1
        |addx 3
        |addx 1
        |noop
        |noop
        |noop
        |addx -9
        |addx 18
        |addx 1
        |addx 2
        |noop
        |noop
        |addx 9
        |noop
        |noop
        |noop
        |addx -1
        |addx 2
        |addx -37
        |addx 1
        |addx 3
        |noop
        |addx 15
        |addx -21
        |addx 22
        |addx -6
        |addx 1
        |noop
        |addx 2
        |addx 1
        |noop
        |addx -10
        |noop
        |noop
        |addx 20
        |addx 1
        |addx 2
        |addx 2
        |addx -6
        |addx -11
        |noop
        |noop
        |noop""".stripMargin
    )).answers
    assertEquals(a1, 13_140)
    assertEquals(a2, """
      |##..##..##..##..##..##..##..##..##..##..
      |###...###...###...###...###...###...###.
      |####....####....####....####....####....
      |#####.....#####.....#####.....#####.....
      |######......######......######......####
      |#######.......#######.......#######.....""".stripMargin)
  }

  test("Day 11") {
    val (a1, a2) = Day11(Some(
      """Monkey 0:
        |  Starting items: 79, 98
        |  Operation: new = old * 19
        |  Test: divisible by 23
        |    If true: throw to monkey 2
        |    If false: throw to monkey 3
        |
        |Monkey 1:
        |  Starting items: 54, 65, 75, 74
        |  Operation: new = old + 6
        |  Test: divisible by 19
        |    If true: throw to monkey 2
        |    If false: throw to monkey 0
        |
        |Monkey 2:
        |  Starting items: 79, 60, 97
        |  Operation: new = old * old
        |  Test: divisible by 13
        |    If true: throw to monkey 1
        |    If false: throw to monkey 3
        |
        |Monkey 3:
        |  Starting items: 74
        |  Operation: new = old + 3
        |  Test: divisible by 17
        |    If true: throw to monkey 0
        |    If false: throw to monkey 1""".stripMargin
    )).answers
    assertEquals(a1, 10_605L)
    assertEquals(a2, 2_713_310_158L)
  }

  test("Day 12") {
    val (a1, a2) = Day12(Some("Sabqponm\nabcryxxl\naccszExk\nacctuvwj\nabdefghi"))
      .answers
    assertEquals(a1, 31)
    assertEquals(a2, 29)
  }
